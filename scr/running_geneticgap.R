####################################################
### Running genetic gap and interpreting results ###
####################################################
#### Credit Olivier Francois & Clement Gain
#### Load packages ####
# devtools::install_github("bcm-uga/lfmm")
# if (!require("BiocManager", quietly=T))
#   install.packages("BiocManager")
# BiocManager::install("LEA", force=TRUE)
# install.packages("viridis")
# install.packages("scales")
# install.packages("vcfR")
library(LEA)
library(viridis)
library(scales)
library(vcfR)
packageDescription("LEA", fields="Date")
sessionInfo()


#### Set variables ####
labsp <- c("Fagus_sylvatica","Pinus_sylvestris")
codesp <- c("FS","PS")
cols <- c("#E69F00","#0072B2")
opt.K <- c(8,3)


#### Set the paths ####
dir.path <- "/Users/dauphin/Sync/Documents/GitLabExt/prj/GenomicOffset/res/"
dat.path1 <- "/Users/dauphin/Sync/Documents/GitLabExt/prj/GenomicOffset/dat/"
dat.path2 <- "/Users/dauphin/Sync/Documents/GitLabExt/prj/GenEnvAssociation/res/lfmm/"


#### Set the working directory ####
setwd(dir.path)


#### Set the species ####
sp <- 1 # if running Fagus sylvatica
#sp <- 2 # if running Pinus sylvestris


#### Import genotype dataset from the vcf file to get SNP and sample identifiers ####
vcf <- read.vcfR(paste(dat.path1,codesp[sp],"_gen_dataset_MAF0.05.vcf.gz",sep=""), verbose=F)
snp.info <- getFIX(vcf)
snp.info <- data.frame(snp.info[,c("CHROM","POS")])
snp.info$SNPid <- paste(snp.info$CHROM,snp.info$POS,sep="_")
dim(snp.info)
sample.ids <- colnames(vcf@gt)[-1]
head(sample.ids); length(sample.ids)


#### Import the genotype dataset with imputed missing data ####
gen <- read.lfmm(paste(dat.path1,codesp[sp],"_gen_dataset_MAF0.05.lfmm_imputed.lfmm",sep=""))
rownames(gen) <- sample.ids


#### Set parameters, folders and create dataframes ####
Y <- gen
dir.create(paste(dir.path, "genetic.gap", sep=""), recursive=F)
dir.create(paste(dir.path, "genetic.gap/", codesp[sp], sep=""), recursive=F)
setwd(paste(dir.path, "genetic.gap/", codesp[sp], sep=""))


#### Import the environmental dataset ####
env <- read.table(paste(dat.path1,codesp[sp],"_env_dataset.csv",sep=""), row.names="sample", header=T, sep=",")


#### Define current and future conditions ####
if(sp == 1) { # for FS
  pred1 <- c("Bio01.Cur","Bio08.Cur","Bio12.Cur","Bio15.Cur")
  pred2 <- c("Bio01.Fut","Bio08.Fut","Bio12.Fut","Bio15.Fut")
} else { # for PS
  pred1 <- c("Bio01.Cur","Bio07.Cur","Bio08.Cur","Bio09.Cur","Bio12.Cur","Bio15.Cur")
  pred2 <- c("Bio01.Fut","Bio07.Fut","Bio08.Fut","Bio09.Fut","Bio12.Fut","Bio15.Fut")
}
X.Cur <- env[,pred1]
X.Fut <- env[,pred2]
dim(X.Cur)[1]==dim(X.Fut)[1]
dim(X.Cur)[2]==dim(X.Fut)[2]


#### Inspect differences between current and future conditions ####
pdf(paste("CorrelationCurrentFutureConditions.pdf", sep=""), width=8.24, height=11.69)
par(mfrow=c(3,2), mar=c(5,5,4,2))
for(i in 1:NCOL(X.Cur)) {
  plot(X.Cur[,i], X.Fut[,i], main=sub(".Cur*","",colnames(X.Cur[i])), pch=16,
       xlab="Current conditions", ylab="Future conditions", col=alpha("darkred",0.2),
       xlim=c(min(c(X.Cur[,i], X.Fut[,i])), max(c(X.Cur[,i], X.Fut[,i]))),
       ylim=c(min(c(X.Cur[,i], X.Fut[,i])), max(c(X.Cur[,i], X.Fut[,i]))))
  abline(coef=c(0,1), lty=2)
}
dev.off()


#### Compute genetic gap from all loci ####
?genetic.gap
g.gap <- genetic.gap(input=Y, env=X.Cur, pred.env=X.Fut, K=opt.K[sp])
head(g.gap$offset)
head(g.gap$distance)
pdf(paste("GenomicOffsetValuesGapVsRONA.pdf", sep=""), width=8.24*0.7, height=8.24*0.7)
par(mfrow=c(1,1))
plot(g.gap$offset,g.gap$distance)
dev.off()
cor(g.gap$offset, g.gap$distance, method="pearson")

res.ind <- data.frame(matrix(NA, nrow=dim(Y)[1], ncol=1))
res.ind[,"sample.id"] <- sample.ids
res.ind[,"pop.id"] <- as.factor(substr(sample.ids, start=4, stop=8))
res.ind[,"offset"] <- g.gap$offset
res.ind[,"distance"] <- g.gap$distance
res.ind <- res.ind[,-1]
write.table(res.ind, "GenomicOffsetValuesInd.csv", sep=",", row.names=F, col.names=T, quote=F)

res.pop <- stats::aggregate(res.ind[,3:4], by=list(res.ind$pop.id), FUN=mean) 
colnames(res.pop)[1] <- "pop.id"
write.table(res.pop, "GenomicOffsetValuesPop.csv", sep=",", row.names=F, col.names=T, quote=F)


#### Plot genetic gap across samples ####
pdf(paste("GenomicOffsetValuesInd.pdf", sep=""), width=8.24*3, height=8.24)
par(mfrow=c(1,1), mar=c(9,5,4,2))
barplot(res.ind$offset, col=as.factor(res.ind$pop.id), names.arg=res.ind$sample.id, las=2)
dev.off()

pdf(paste("GenomicOffsetValuesPop.pdf", sep=""), width=8.24*3, height=8.24)
barplot(res.pop$offset, col=as.factor(res.pop$pop.id), names.arg=res.pop$pop.id, las=2)
dev.off()


#### Plot the genetic gap vs Euclidean environmental distance ####
delta <- X.Cur - X.Fut
dist.env <- sqrt(rowSums(delta^2))  
pdf(paste("CorrelationGeneticGapAllLociAndEuclideanDistance.pdf", sep=""), width=8.24, height=8.24)
par(mfrow=c(1,1))
plot(dist.env, g.gap$offset, xlab="Euclidean environmental distance", ylab ="genetic gap", cex=0.6, col="blue")
dev.off()
cor(dist.env, g.gap$offset, method="spearman")


#### Compute genetic gap from candidate loci ####
if(sp == 1) { # for FS
  lfmm.res <- read.table(paste(dat.path2,codesp[sp],"/env4_Bio15.Cur/K",opt.K[sp],"/LFMM_AllResults_env4_Bio15.Cur_K",opt.K[sp],".csv",sep=""), header=T, sep=",")
} else { # for PS
  lfmm.res <- read.table(paste(dat.path2,codesp[sp],"/env6_Bio15.Cur/K",opt.K[sp],"/LFMM_AllResults_env6_Bio15.Cur_K",opt.K[sp],".csv",sep=""), header=T, sep=",")
}
candidates <- which(lfmm.res$qvalue < 0.05)
g.gap.candidate <- genetic.gap(input=Y, env=X.Cur, pred.env=X.Fut, K=opt.K[sp], candidate.loci=candidates)


#### Plot the correlation of genetic gap between all and candidate loci ####
pdf(paste("CorrelationGeneticGapAllLociAndCandidateLoci.pdf", sep=""), width=8.24, height=8.24)
plot(g.gap$offset, g.gap.candidate$offset, cex=0.6, col="red") # there is a relatively high correlation
dev.off()
cor(g.gap$offset, g.gap.candidate$offset, method="spearman")


#### Assess the importance of environmental variables using scaling ####
g.gap.scaled <- genetic.gap(input=Y, env=X.Cur, pred.env=X.Fut, K=opt.K[sp], scale=T)


#### Plot the correlation of genetic gap between scaled and non-scaled predictors ####
pdf(paste("CorrelationGeneticGapAllLociWithScaledAndNonScaledPredictors.pdf", sep=""), width=8.24, height=8.24)
plot(g.gap$offset, g.gap.scaled$offset, cex=0.6, col="orange") # scaling does not change genetic gaps
dev.off()
cor(g.gap$offset, g.gap.scaled$offset, method="spearman")


#### Inspect dimensions of the environmental space influence the genetic gap ####
pdf(paste("EingenvaluesWithScaledAndPredictors.pdf", sep=""), width=8.24, height=8.24)
barplot(g.gap.scaled$eigenvalues, col="pink", xlab="Axes", ylab="Eigenvalues") # only three dimensions of the environmental space influence the genetic gap
dev.off()
g.gap.scaled$vectors[,1:2] # loadings for the first two combinations of variables indicate their relative contribution to local adaptation
